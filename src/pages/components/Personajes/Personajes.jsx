import React, { useState, useEffect, useContext } from 'react';
import { PersonajesGallery } from './PersonajesGallery';
import littleHouse from '../../../styles/images/Group.svg';


export default function Personajes () {

    return (
       
        <div>
         <div>
        <img className='littleHouse' src={littleHouse} alt='littleHouse'/>
        </div>
        <div>
            <PersonajesGallery />
        </div>
        </div>

    );
}
