import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
} from "react-router-dom";
import { PersonajeElegidoContext } from "../../../shared/contexts/PersonajeElegidoContext";
import { PersonajesContext } from "../../../shared/contexts/PersonajesContext";
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import './Personajes.scss';

export function PersonajesGallery() {
  const [personajeElegido, setPersonajeElegido] = useContext(
    PersonajeElegidoContext
  );
  const [personajes, setPersonajes] = useContext(PersonajesContext);

  return (

    <div className="personajes-body">
        <SimpleBar style={{ maxHeight: 700, maxWidth:1800}}>
            <div  className="row">
                {personajes.map((item, index) => (
                    item.image &&
                <div className="main-figure col-md-2 col-4"  key={index}>
                    <figure onClick={() => {
                        setPersonajeElegido(item);
                    }}
                    >
                    <Link className="text-decoration__none" to={"personajes-detail/" + item.name}>
                        <img className="images" src={item.image} alt={item.name}>
                        </img>
                        <p className="text-small-s overlay">{item.name}</p>
 
                    </Link>
                    </figure>
                </div>
                ))}

            </div>
            </SimpleBar>

    </div>
  );
}
