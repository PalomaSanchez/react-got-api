import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route, Link, Redirect, useParams } from "react-router-dom";
import { PersonajeElegidoContext } from '../../../shared/contexts/PersonajeElegidoContext'
import { PersonajesContext } from '../../../shared/contexts/PersonajesContext';
import './PersonajesDetail.scss';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

export default function PersonajesDetail () {
    
    const name = useParams().name;
    const [personajeElegido, setPersonajeElegido] = useContext(PersonajeElegidoContext);
    const [personajes, setPersonajes] = useContext(PersonajesContext);
    const [logoCasa, setLogoCasa] = useState('');

    useEffect(() => {
        if (personajes.length < 1){
            axios.get(process.env.REACT_APP_BACK_URL + 'characters')
            .then((res) => {
                const salida = res.data;
                setPersonajes(salida);

                const nueva = salida.find(item => item.name === name);
                setPersonajeElegido(nueva);

                axios.get(process.env.REACT_APP_BACK_URL + 'houses/' + nueva.house)
                .then((res) => {
                    const imagen = res.data[0].logoURL;
                    
                    setLogoCasa(imagen);
                    
                })
                .catch((error) => {
                    console.log(error);
                })
            })
                .catch((error) => {
                    console.log(error);
            });
        }else{
            const nueva = personajes.find(item => item.name === name);
            setPersonajeElegido(personajes.find(item => item.name === name));

            axios.get(process.env.REACT_APP_BACK_URL + 'houses/' + personajeElegido.house)
                .then((res) => {
                    console.log(res);
                    const imagen = res.data[0].logoURL;
                    
                    setLogoCasa(imagen);
                    
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        
       
    }, []);

    return (
        <div className="personajes">
            <div className="personajes-titulo">
                <figure>
                    <img className="personajes-titulo__img" src={personajeElegido.image} alt={personajeElegido.name}></img>
                    <figcaption className="personajes-titulo__caption">
                        <p className="text-large">{personajeElegido.name}</p>
                    </figcaption>
                </figure>
            </div>

            <div className="personaje-detalles">
                <div className="personaje-detalles__caja" id="sigil">
                    <p className="text-medium">Casa</p>
                    <div className="personaje-detalles__img">
                            {personajeElegido && personajeElegido.house ? 
                                <img src={logoCasa} alt="Sigil"/>
                                : <p></p>}
                    </div> 
                </div>

                <div className="personaje-detalles__caja" id="alianzas">
                    <p className="text-medium">Alianzas</p>
                    <div className="personaje-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {personajeElegido && personajeElegido.allegiances ? 
                                    personajeElegido.allegiances.map((item, index) => 
                                    <p className="text-small" key={index}>{item}</p>
                                    )  
                                    : <p></p>}
                        </SimpleBar>
                    </div> 
                </div>

                <div className="personaje-detalles__caja" id="apariciones">
                    <p className="text-medium">Apariciones</p>
                    <div className="personaje-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {personajeElegido && personajeElegido.appearances ? 
                                    personajeElegido.appearances.map((item, index) => 
                                    <p className="text-small" key={index}>{item}</p>
                                    )  
                                    : <p></p>}
                        </SimpleBar>
                    </div> 
                </div>

                <div className="personaje-detalles__caja" id="padre">
                    <p className="text-medium">Padre</p>
                    <div className="personaje-detalles__lista">
                            {personajeElegido && personajeElegido.father ? 
                                <p className="text-small">{personajeElegido.father}</p>
                                : <p></p>}
                    </div> 
                </div>

                <div className="personaje-detalles__caja" id="hermanos">
                    <p className="text-medium">Hermanos</p>
                    <div className="personaje-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {personajeElegido && personajeElegido.siblings ? 
                                    personajeElegido.siblings.map((item, index) => 
                                    <p className="text-small" key={index}>{item}</p>
                                    )  
                                    : <p></p>}
                        </SimpleBar>
                    </div> 
                </div>

                <div className="personaje-detalles__caja" id="titulos">
                    <p className="text-medium">Titulos</p>
                    <div className="personaje-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {personajeElegido && personajeElegido.titles ? 
                                    personajeElegido.titles.map((item, index) => 
                                    <p className="text-small" key={index}>{item}</p>
                                    )  
                                    : <p></p>}
                        </SimpleBar>
                    </div> 
                </div>

            </div>
        </div>
    );
}