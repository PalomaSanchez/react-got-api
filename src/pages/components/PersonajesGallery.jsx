import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
} from "react-router-dom";
import { PersonajeElegidoContext } from "../../shared/contexts/PersonajeElegidoContext";
import { PersonajesContext } from "../../shared/contexts/PersonajesContext";
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';


export function PersonajesGallery() {
  const [personajeElegido, setPersonajeElegido] = useContext(
    PersonajeElegidoContext
  );
  const [personajes, setPersonajes] = useContext(PersonajesContext);

  return (
    <div>
    <SimpleBar style={{ maxHeight: 520 }}>
      <div className="row">
      <div class="col-8 col-md-6 col-4">
        {personajes.map((item, index) => (
          <div  key={index}>
            <figure  onClick={() => { setPersonajeElegido(item);}}>
              <Link to={"personajes-detail/" + item.name}>
                <img className="images" src={item.image} alt={item.name}></img>
                <figcaption>{item.name}</figcaption>
              </Link>
            </figure>
          </div>
        ))}
        </div>

      </div>
      </SimpleBar>
    </div>
  );
}
