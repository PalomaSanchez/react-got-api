import React, {useState, useContext, useEffect} from 'react';
import { CasasGaleryContext } from '../../../shared/contexts/CasasGaleryContext';
import axios from 'axios';
import { CasaDetailContext } from '../../../shared/contexts/CasaDetailContext';
import { BrowserRouter as Router, Switch, Route, Link, Redirect, useParams } from "react-router-dom";
import './CasasDetail.scss';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

export default function CasasGalery () {

    const [casas, setCasas] = useContext(CasasGaleryContext);
    const [casaDetail, setCasaDetail] = useContext(CasaDetailContext);
    const name = useParams().tail;

    useEffect(() => {
        if(casas.length < 1){
            axios.get(process.env.REACT_APP_BACK_URL + 'houses')
            .then((res) => {
                const salida = res.data;
                setCasas(salida);
                setCasaDetail(salida.find(item => item.name == name));
            })
            .catch((error) => {
                console.log(error);
            });
            
        }else{
            setCasaDetail(casas.find(item => item.name == name));
        }

    }, []);


    return (
        <div className="casas">
            <div className="casas-titulo">
                <figure className="casas-titulo__figure">
                    <img className="casas-titulo__img" src={casaDetail.logoURL} alt={casaDetail.name}></img>
                    <figcaption className="casas-titulo__caption">
                        <p className="text-large">{casaDetail.name}</p>
                    </figcaption>
                </figure>
            </div>

            <div className="casas-detalles">
                <div className="casas-detalles__caja" id="lema">
                    <p className="text-medium">Lema</p>
                    <div className="casas-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {casaDetail && casaDetail.sigil ? 
                                    <p className="text-small">{casaDetail.sigil}</p>
                                : <p></p>}
                        </SimpleBar>
                    </div> 
                </div>

                <div className="casas-detalles__caja" id="sede">
                    <p className="text-medium">Sede</p>
                    <div className="casas-detalles__lista">
                        {casaDetail && casaDetail.seat ? 
                                <p className="text-small">{casaDetail.seat}</p>
                            : <p></p>}
                    </div>
                </div>

                <div className="casas-detalles__caja" id="region">
                    <p className="text-medium">Region</p>
                    <div className="casas-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {casaDetail && casaDetail.region ? casaDetail.region.map((item, index) => 
                                    <p className="text-small" key={ item + index}>{item}</p>
                                ) : <p></p>}
                        </SimpleBar>  
                    </div>
                </div>

                <div className="casas-detalles__caja" id="alianza">
                    <p className="text-medium">Alianzas</p>
                    <div className="casas-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {casaDetail && casaDetail.allegiance ? casaDetail.allegiance.map((item, index) => 
                                    <p className="text-small" key={item + index}>{item}</p>
                                ) : <p></p>}
                        </SimpleBar>  
                    </div>
                </div>
                
                <div className="casas-detalles__caja" id="religion">
                    <p className="text-medium">Religiones</p>
                    <div className="casas-detalles__lista">
                        <SimpleBar style={{ maxHeight: 220}}>
                            {casaDetail && casaDetail.religion ? casaDetail.religion.map((item, index) => 
                                    <p className="text-small" key={item + index}>{item}</p>
                                ) : <p></p>}
                        </SimpleBar>
                    </div>
                </div>

                <div className="casas-detalles__caja" id="fundacion">
                    <p className="text-medium">Fundacion</p>
                    <div className="casas-detalles__lista">
                        {casaDetail && casaDetail.createdAt ? 
                                <p className="text-small" >{casaDetail.createdAt}</p>
                            : <p></p>}
                    </div>
                </div>

            </div>
        </div>
    );
}