import React, {useState, useContext} from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect, useParams } from "react-router-dom";
import { CasasGaleryContext } from '../../../shared/contexts/CasasGaleryContext';
import './CasasGalery.scss';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

export default function CasasGalery () {

    const [casas, setCasas] = useContext(CasasGaleryContext);

    return (
        <div className="galery">
            <SimpleBar style={{ maxHeight: 700, maxWidth:1800,  color:'white'}}>
                <div className="casas-galery">
                    {casas.map((casa, index) => {
                        if(casa.logoURL){
                            return(
                            <div className="casas-galery__casa">
                                <Link className="text-decoration__none" to={'casa-detail/' + casa.name}>
                                    <figure className="casas-galery__figure">
                                        <img className="casas-galery__img" key={index} src={casa.logoURL} alt={casa.name}></img>
                                        <figcaption className="casas-galery__caption">
                                            <p className="text-small-s">{casa.name}</p>
                                        </figcaption>
                                    </figure>
                                </Link>
                            </div>)
                        }                
                    })}
                </div>
            </SimpleBar>
        </div>
    );
}