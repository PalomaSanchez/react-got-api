import React, {useState} from 'react';
import CasasGalery from './CasasGalery';
import littleHouse from '../../../styles/images/Group.svg';

export default function Casas () {

    

    return (
        <div className="row">
        <div>
        <img className='littleHouse' src={littleHouse} alt='littleHouse'/>
        </div>
            <div className='casas'>
            <CasasGalery></CasasGalery>
            </div>
            </div>


    );
}