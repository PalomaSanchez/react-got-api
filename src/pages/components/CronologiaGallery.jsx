import React, {useContext, useState} from 'react';
import * as _ from 'lodash';

import {CronologiaGalleryContext} from '../../shared/contexts/CronologiaGalleryContext';
import {PersonajesContext} from "../../shared/contexts/PersonajesContext";

import '../.././styles/CronologiaGallery.scss';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import littleHouse from '../.././styles/images/Group.svg';



const CronologiaGallery = props => {
    const [personajes, setPersonajes] = useContext(PersonajesContext);
    const [edadPersonajes, setEdadPersonajes] = useContext(CronologiaGalleryContext);

    function ageSortHandler(){
        _.orderBy(edadPersonajes?.age, ['age'], ['desc'])
    }

    return (
        <div>
         <div>
        <img className='littleHouse' src={littleHouse} alt='littleHouse'/>
        </div>
        <SimpleBar style={{ maxHeight: 700, maxWidth:1800}}>
∑        <React.Fragment>

            <div className="container1">
                <button className="item1" onClick={ageSortHandler}>🔘</button>
            </div>

            {_.orderBy(edadPersonajes, ['age'], ['asc']).map((item, index) =>
                item.age &&
                <div key={index} className="container2">
                    <div className="item2">{item.age} <br /> {item.name} <br />
                        <img src={item.image} alt="avatar" className="avatar-img"/></div>
                    <div className="item2"></div>
                </div>
            )}
        </React.Fragment>
        </SimpleBar>
        </div>
    );
}
export default CronologiaGallery;


