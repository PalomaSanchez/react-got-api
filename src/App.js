
import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Link, Redirect, Route, Switch } from "react-router-dom";
import Personajes from './pages/components/Personajes/Personajes';
import Casas from './pages/components/Casas/Casas';
import CronologiaGallery from './pages/components/CronologiaGallery';
import PersonajesDetail from './pages/components/Personajes/PersonajesDetail';
import CasaDetail from './pages/components/Casas/CasaDetail';
import HomePage from './pages/HomePage';
import { PersonajesContext } from './shared/contexts/PersonajesContext';
import { PersonajeElegidoContext } from './shared/contexts/PersonajeElegidoContext';
import axios from 'axios';
import { CasasGaleryContext } from './shared/contexts/CasasGaleryContext';
import { CronologiaGalleryContext } from './shared/contexts/CronologiaGalleryContext';
import { CasaDetailContext } from './shared/contexts/CasaDetailContext';
import Cronologia from "./pages/components/Cronologia";
import "./styles/HomePage.scss";
import ukflag from './styles/images/united-kingdom1.png';
import spnflag from './styles/images/spain1.png';
import './App.scss';


function App() {
  const [personajeElegido, setPersonajeElegido] = useState({});
  const [personajes, setPersonajes] = useState([]);
    const [edadPersonajes, setEdadPersonajes] = useState([]);
  const [casas, setCasas] = useState([]);
  const [casaDetail, setCasaDetail] = useState([]);

  useEffect(() => {
    if (personajes.length === 0) {
      axios
        .get(process.env.REACT_APP_BACK_URL + "characters")
        .then((res) => {
          setPersonajes(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, []);

  useEffect(() => {
    if (casas.length === 0) {
      axios
        .get(process.env.REACT_APP_BACK_URL + "houses")
        .then((res) => {
          setCasas(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, []);

    useEffect(() => {
        if (edadPersonajes.length === 0){
            axios.get(process.env.REACT_APP_BACK_URL + 'ages')
                .then((res) => {
                    setEdadPersonajes(res.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }, []);

  return (
    <div className="App">
    <header>
    <div className='Flags'>
          <img src={ukflag} alt='ukflag'/>
          <img src={spnflag} alt='spnflag'/>
        </div>
    </header>
      <body>
        <Router>
          <Switch>
            <CronologiaGalleryContext.Provider value={[edadPersonajes, setPersonajes]}>
                <PersonajesContext.Provider value={[personajes, setEdadPersonajes]}>
                    <PersonajeElegidoContext.Provider value = {[personajeElegido, setPersonajeElegido]}>
                        <Route path="/homePage">
                            <HomePage />
                        </Route>

                        <Route path="/personajes-detail/:name">
                          <PersonajesDetail />
                        </Route>

                        <Route path="/personajes">
                          <Personajes />
                        </Route>

                        <CasasGaleryContext.Provider value={[casas, setCasas]}>
                            <CasaDetailContext.Provider value={[casaDetail, setCasaDetail]}>
                                <Route path="/casas">
                                  <Casas />
                                </Route>
                                <Route path="/casa-detail/:tail">
                                    <CasaDetail />
                                </Route>
                            </CasaDetailContext.Provider>
                        </CasasGaleryContext.Provider>
              
              
                        <Route path="/cronologia">
                            <CronologiaGallery />
                        </Route>


                    </PersonajeElegidoContext.Provider>
                </PersonajesContext.Provider>
            </CronologiaGalleryContext.Provider>
          </Switch>
          <footer>
            <nav className="nav">
            <div className='row'>
            <div className="menu-bottom">
              <ul className="ul">
                <li className="nav-list__item">
                  <Link className="" to="/personajes">
                    Personajes
                  </Link>
                </li>
                <li className="nav-list__item">
                  <Link className="" to="/casas">
                    Casas
                  </Link>
                </li>
                <li className="nav-list__item">
                  <Link className="" to="/cronologia">
                    Cronologia
                  </Link>
                </li>
                <li className="nav-list__item">
                  <Link className="" to="/homePage">
                    Home
                  </Link>
                </li>
              </ul>
              </div>
              </div>
            </nav>
          </footer>
        </Router>
      </body>
    </div>
  );
}

export default App;
